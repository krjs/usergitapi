import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { UserListComponent } from './user-list/user-list.component';
import { GhApiReposService } from './services/gh-api-repos.service';
import { ContributorsService } from './services/contributors.service';
import { UserProfileService } from './services/user-profile.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { PageNotFountComponent } from './page-not-fount/page-not-fount.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { RepoDetailsComponent } from './repo-details/repo-details.component';


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserDetailsComponent,
    PageNotFountComponent,
    RepoDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [
    GhApiReposService, 
    ContributorsService, 
    UserProfileService],
  bootstrap: [AppComponent]
})
export class AppModule { }
