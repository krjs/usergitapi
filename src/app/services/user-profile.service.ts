import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  private accessToken = '750b41a60c5ae7de317b956fe37cdae3cf871f55';

  constructor(private _http: HttpClient) {}

  getUserProfile(_userName) {
    return this._http.get<any>('https://api.github.com/users/'+ _userName +'?access_token='+this.accessToken)
   }
}
