import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { gitRepos } from '../interfaces/repoInterface';

@Injectable({
  providedIn: 'root'
})
export class GhApiReposService {

  private accessToken = '750b41a60c5ae7de317b956fe37cdae3cf871f55';

  constructor(private _http: HttpClient) {}

   getRepos():Observable<gitRepos[]> {
     return this._http.get<gitRepos[]>('https://api.github.com/orgs/angular/repos?access_token='+this.accessToken) 
    }             
}
