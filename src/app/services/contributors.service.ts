import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { gitContributors } from '../interfaces/contributorsInterface';

@Injectable({
  providedIn: 'root'
})
export class ContributorsService {

  private accessToken = '750b41a60c5ae7de317b956fe37cdae3cf871f55';
  constructor(private _http: HttpClient) {}

   getContributors(_projectName){
     if(_projectName !== undefined) {
      return this._http.get<gitContributors[]>('https://api.github.com/repos/' + _projectName +'/contributors?access_token='+this.accessToken)
     }
   }
}
