import { TestBed, inject } from '@angular/core/testing';

import { GhApiReposService } from './gh-api-repos.service';

describe('GhApiReposService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GhApiReposService]
    });
  });

  it('should be created', inject([GhApiReposService], (service: GhApiReposService) => {
    expect(service).toBeTruthy();
  }));
});
