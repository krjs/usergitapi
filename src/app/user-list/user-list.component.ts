import { Component, OnInit } from '@angular/core';
import { GhApiReposService } from '../services/gh-api-repos.service';
import { ContributorsService } from '../services/contributors.service';
import { UserProfileService } from '../services/user-profile.service';
import { Router } from '@angular/router'
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  private fullRepoList = [];
  public repoNameList = [];
  public contributorsList = [];
  public reposWithContributors = [];
  public userProfileList = [];
  public blockFlag: boolean = false;
  public REPO_CACHE_KEY = 'REPO_CACHE';
  public REPO_CON_KEY = 'CONTRIBUTORS_LIST'
  public USER_CACHE_KEY = 'USER_LIST';
  public REPO_USER_CACHE_KEY = 'REPO_USER';
  public initDataObs: Observable<any>;;
  public contributorsListObs: Observable<any>;
  public userProfileObs:Observable<any>; 
  public userCacheFlag;;

  constructor(
    private _ghRepoService: GhApiReposService,
    private _ghContributorList: ContributorsService,
    private _ghUserProfile: UserProfileService,
    private _router: Router,
  ) {
    this.fullRepoList = [];
  }

  ngOnInit() {
    let checkCacheUserList = this.takeDataFromLS(this.USER_CACHE_KEY);

    if(checkCacheUserList.length === 0) {
      this.getDataFromApi()
    } else {
      this.userProfileList = this.takeDataFromLS(this.USER_CACHE_KEY)
      this.userCacheFlag = true;
    }
  }

  getDataFromApi() {
    this.initDataObs = this._ghRepoService.getRepos();

    this.initDataObs.subscribe((data) => {
      this.fullRepoList = data;
    }, error => {
      console.log(error)
    }, () => {
      localStorage[this.REPO_CACHE_KEY] = JSON.stringify(this.fullRepoList)
      this.getOnlyRepoName(this.repoNameList, this.fullRepoList);
    })
  }

  getOnlyRepoName(param, repo) {
    repo.filter(i => {
      if (i !== undefined || i !== 'undefined') {
        param.push(i.full_name)
      }
    })

    this.getContributorsFromRepo()
  }

  takeDataFromLS(lskey) {
    return JSON.parse(localStorage.getItem(lskey) || '[]')
  }

  getContributorsFromRepo() {
    for (let i = 0; i < this.repoNameList.length; i++) {
      this.contributorsListObs = this._ghContributorList.getContributors(this.repoNameList[i]);
      this.contributorsListObs
        .subscribe(
          data => {
            let filteredData = {
              repoName: this.repoNameList[i],
              repoContributors: data.map(i => { return i.login })
            }

            if (i === this.repoNameList.length - 1) {
              this.blockFlag = true;
            }

            this.putDataInArray(filteredData);

          }
        ), (error) => {
          console.log(error)
        }
    }
  }


  findContributors(param) {
    let arrayRepoWithContributors = param,
      newArrayWithContributtorsOnly = [],
      arrayWithoutDuplicateContributor = [];

    newArrayWithContributtorsOnly = arrayRepoWithContributors.map(
      elem => {
        return elem[1];
      }
    )

    localStorage[this.REPO_USER_CACHE_KEY] = JSON.stringify(arrayRepoWithContributors)
    arrayWithoutDuplicateContributor = [].concat.apply([], newArrayWithContributtorsOnly);

    this.contributorsList = arrayWithoutDuplicateContributor.filter(function (item, index) {
      return arrayWithoutDuplicateContributor.indexOf(item) >= index;
    });

    this.getUserProfileData(this.contributorsList);
  }

  getUserProfileData(array) {

    if(this.userCacheFlag ){
      this.userProfileList = [];
      this.userCacheFlag = false
    }

    array.map(eachUserProfile => {
      this.userProfileObs = this._ghUserProfile.getUserProfile(eachUserProfile)

      this.userProfileObs.subscribe(data => {
        this.userProfileList.push(data)
        localStorage[this.USER_CACHE_KEY] = JSON.stringify(this.userProfileList)
      }), (error) => {
        console.log(error)
      }
    })

  }

  sortedByFallowers() {
    this.userProfileList.sort((param1, param2) => {
      return param2.followers - param1.followers
    })
  }

  sortedByAmountOfPublicRepo() {
    this.userProfileList.sort((param1, param2) => {
      return param2.public_repos - param1.public_repos
    })
  }

  sortedByAmountOfGist() {
    this.userProfileList.sort((param1, param2) => {
      return param2.public_gists - param1.public_gists
    })
  }

  putDataInArray(data) {
    let passToArry = Object.values(data)
    this.reposWithContributors.push(passToArry)

    if (this.blockFlag) {
      this.findContributors(this.reposWithContributors)
      this.blockFlag = false;
    }
  }

  selectedUser(user) {
    this._router.navigate(['user', user.login])
  }
}
