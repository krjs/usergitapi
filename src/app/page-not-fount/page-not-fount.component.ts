import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'

@Component({
  selector: 'app-page-not-fount',
  templateUrl: './page-not-fount.component.html',
  styleUrls: ['./page-not-fount.component.css']
})
export class PageNotFountComponent implements OnInit {

  constructor( private _router: Router ) { }

  ngOnInit() {
  }
}
