import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'
import { UserProfileService } from '../services/user-profile.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  public userLogin;
  public repoWithUsers;
  public reposSelected;
  public usersData;
  public selectedUserData;

  constructor(
    private _route: ActivatedRoute, 
    private _router: Router,
    private _userData:UserProfileService ) { 
  }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) =>{
      let user = params.get('userlogin');
      this.userLogin = user;
    }), (error) => {
      console.log(error)
    }

    if(this.usersData === null || this.usersData === undefined) {
      this.getDataFromApi(this.userLogin)
    } 
    
    this.usersData = JSON.parse(localStorage.getItem('USER_LIST'));
    this.repoWithUsers = JSON.parse(localStorage.getItem('REPO_USER'));
    this.findUserData(this.userLogin)
    this.findReposUsersContributed(this.userLogin, this.repoWithUsers)
  }

  goToHomePage() {
    this._router.navigate(['home'])
  }

  getDataFromApi(user) {
    this._userData.getUserProfile(user)
    .subscribe(data => {
      this.selectedUserData = data
    }) , (error) => {
      console.log(error)
    }
  }

  findUserData(user) {
    this.usersData.map (
      elem => {
        if (elem.login === user) {
          this.selectedUserData = elem
        }
      }
    )
  }

  findReposUsersContributed(user, repos){
    let arrayWithRepos = [];
    repos.filter(
      item => {
       item[1].filter(
          userlist =>{
            if(userlist === user){
              arrayWithRepos.push(item[0])
            }
          }
        ) 
        
      }
    )

    this.reposSelected = arrayWithRepos;
  }

  goToRepoDetails(repo) {    
    this._router.navigate(['repodetails', repo])
  }
}
