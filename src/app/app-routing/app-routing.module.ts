import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from '../user-list/user-list.component';
import { UserDetailsComponent } from '../user-details/user-details.component';
import { PageNotFountComponent } from '../page-not-fount/page-not-fount.component';
import { RepoDetailsComponent } from '../repo-details/repo-details.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component : UserListComponent },
  { path: 'user/:userlogin', component : UserDetailsComponent },
  { path: 'repodetails/:reponame', component : RepoDetailsComponent },
  { path: '**', component : PageNotFountComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
