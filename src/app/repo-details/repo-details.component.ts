import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router'

@Component({
  selector: 'app-repo-details',
  templateUrl: './repo-details.component.html',
  styleUrls: ['./repo-details.component.css']
})
export class RepoDetailsComponent implements OnInit {
  public repoName;
  public repoDetailsData;
  public repoListCache;
  public repoContributors;

  constructor(
    private _route: ActivatedRoute, 
    private _router: Router,
  ) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) =>{
      let repoName = params.get('reponame');
      this.repoName = repoName;
    }), (error) => {
      console.log(error)
    }

    this.repoListCache = JSON.parse(localStorage.getItem('REPO_CACHE'));
  
    if(this.repoListCache === null){
      console.log('Sorry some issue with data')
    } else {
      this.getRepoData(this.repoListCache)
    }
  }

  getRepoData(repos){
    repos.map(
      elem => {
        if(elem.full_name === this.repoName){
          this.repoDetailsData = elem;
        }
      }
    )

  this.getRepoContributors(this.repoName)
  }

  getRepoContributors(repoName) {
    let reposUsers = JSON.parse(localStorage.getItem('REPO_USER'));

    reposUsers.map(
      elem => {
        if (elem[0] === repoName) {
          this.repoContributors = elem[1];
        }
      }
    )
  }

  goToContributor(user) {
    this._router.navigate(['user', user])
  }

  goToHomePage() {
    this._router.navigate(['home'])
  }
}
